﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Database;
using Sprawdzian3GuessNumber;

namespace Logic
{
    public class GameLogic
    {
        private GuessGameDbContext _dbContext;
        private HighScoreRepo _highScoreRepo;
        private int _randomAnswer;
        public int NumberOfGuesses { get; set; }
        public List<HighScore> HighScores = new List<HighScore>();

        public GameLogic()
        {
            _dbContext = new GuessGameDbContext();
            _highScoreRepo = new HighScoreRepo(_dbContext);
            _randomAnswer = new Random().Next(0, 1000);
        }

        public string Guess(string name, int guess)
        {
            int answer = _randomAnswer;

            while (true)
            {
                NumberOfGuesses++;

                switch (guess)
                {
                    case int n when (n > answer):
                        Console.WriteLine(MoreOrLess.Less.ToString());
                        break;

                    case int n when (n < answer):
                        Console.WriteLine(MoreOrLess.More.ToString());
                        break;

                    case int n when (n == answer):
                        HighScore highScore = new HighScore();
                        highScore.Name = name;
                        highScore.Score = NumberOfGuesses;
                        _highScoreRepo.AddScore(highScore);
                        //_highScoreRepo.CreateHighScore(highScore);

                        return $"{MoreOrLess.Correct.ToString()}. You Win!";
                }

                Console.WriteLine("\nGuess again");
                guess = CheckException.CheckInt(Console.ReadLine(), 1, 1000);
            }
        }

        public void Display()
        {
            //List<Hero> Heroes = _heroRepo.RetriveHeroes();
            List<HighScore> highScores = _highScoreRepo.GetScore();

            foreach (var highscore in highScores)
            {
                Console.WriteLine($"\n{highscore.Name}: {highscore.Score}");
            }
        }


    }
}
