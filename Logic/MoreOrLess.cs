﻿namespace Logic
{
    public enum MoreOrLess
    {
        Empty,
        More,
        Less,
        Correct
    }
}