﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sprawdzian3GuessNumber
{
    public class CheckException
    {
        public static int CheckInt(string value, int minNumber, int maxNumber)
        {
            bool IsValid = int.TryParse(value, out int result);
            while (!IsValid || result < minNumber || result > maxNumber)
            {
                Console.WriteLine("Invalid value, please give integer number");
                value = Console.ReadLine();
                IsValid = int.TryParse(value, out result);
            }

            return result;
        }

        public static string CheckName(string name)
        {
            while (string.IsNullOrEmpty(name))
            {
                Console.WriteLine("Please type name");
                name = Console.ReadLine();
            }

            return name;
        }

    }
}
