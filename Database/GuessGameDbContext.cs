﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    public class GuessGameDbContext : DbContext
    {
        public GuessGameDbContext() : base("GuessGameDbConnectionString")
        {
            
        }

        public DbSet<HighScore> DbHighScores;
    }
}
