﻿using System.Collections.Generic;
using System.Linq;

namespace Database
{
    public class HighScoreRepo
    {
        private GuessGameDbContext _dbContext;

        public HighScoreRepo(GuessGameDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void AddScore(HighScore score)
        {
            _dbContext.DbHighScores.Add(score);
            _dbContext.SaveChanges();
        }

        public List<HighScore> GetScore()
        {
            return _dbContext.DbHighScores.ToList();
        }
    }

}