﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using Logic;

namespace Sprawdzian3GuessNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            bool doContinue = true;

            Console.WriteLine("Welcome to the GUESS NUMBER THE GAME.");

            while (doContinue)
            {
                GameLogic gameLogic = new GameLogic();
                Console.WriteLine("\nWhat do you want to do?" +
                                  "\n1.Start Game" +
                                  "\n2.High Scores" +
                                  "\n3.Quit");

                int choice = CheckException.CheckInt(Console.ReadLine(), 1, 3);
                
                switch (choice)
                {
                    case 1:
                        Console.Write("Enter your name: ");
                        string name = CheckException.CheckName(Console.ReadLine());
                        Console.WriteLine("Guess number between 1 and 1000");
                        int guess = CheckException.CheckInt(Console.ReadLine(), 1, 1000);
                        gameLogic.Guess(name, guess);
                        gameLogic.Display();
                        break;

                    case 2:
                        gameLogic.Display();
                        break;
                    case 3:
                        doContinue = false;
                        break;

                }
            }
        }
    }
}
